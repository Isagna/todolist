<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200422174214 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE to_do_list_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE to_do_list_item_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE to_do_list (id INT NOT NULL, last_updated TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE to_do_list_item (id INT NOT NULL, to_do_list_id INT DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, content TEXT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_B3FB63A6B3AB48EB ON to_do_list_item (to_do_list_id)');
        $this->addSql('ALTER TABLE to_do_list_item ADD CONSTRAINT FK_B3FB63A6B3AB48EB FOREIGN KEY (to_do_list_id) REFERENCES to_do_list (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE todolist_user ADD to_do_list_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE todolist_user ADD CONSTRAINT FK_C14A04A0B3AB48EB FOREIGN KEY (to_do_list_id) REFERENCES to_do_list (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C14A04A0B3AB48EB ON todolist_user (to_do_list_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE todolist_user DROP CONSTRAINT FK_C14A04A0B3AB48EB');
        $this->addSql('ALTER TABLE to_do_list_item DROP CONSTRAINT FK_B3FB63A6B3AB48EB');
        $this->addSql('DROP SEQUENCE to_do_list_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE to_do_list_item_id_seq CASCADE');
        $this->addSql('DROP TABLE to_do_list');
        $this->addSql('DROP TABLE to_do_list_item');
        $this->addSql('DROP INDEX UNIQ_C14A04A0B3AB48EB');
        $this->addSql('ALTER TABLE todolist_user DROP to_do_list_id');
    }
}
