<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ToDoListRepository")
 */
class ToDoList
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ToDoListItem", mappedBy="toDoList")
     */
    private $items;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\DateTime
     */
    private $lastUpdated;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", mappedBy="toDoList", cascade={"persist", "remove"})
     */
    private $owner;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|ToDoListItem[]
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    public function addItem(ToDoListItem $item): self
    {
        if (!$this->items->contains($item)) {
            $this->items[] = $item;
            $item->setToDoList($this);
        }

        return $this;
    }

    public function removeItem(ToDoListItem $item): self
    {
        if ($this->items->contains($item)) {
            $this->items->removeElement($item);
            // set the owning side to null (unless already changed)
            if ($item->getToDoList() === $this) {
                $item->setToDoList(null);
            }
        }

        return $this;
    }

    public function getLastUpdated(): ?\DateTimeInterface
    {
        return $this->lastUpdated;
    }

    public function setLastUpdated(?\DateTimeInterface $lastUpdated): self
    {
        $this->lastUpdated = $lastUpdated;

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        // set (or unset) the owning side of the relation if necessary
        $newToDoList = null === $owner ? null : $this;
        if ($owner->getToDoList() !== $newToDoList) {
            $owner->setToDoList($newToDoList);
        }

        return $this;
    }

    public function canAddItem (ToDoListItem $item) {
        $limit = 10;
        if ( count($this->getItems()) < $limit) {
            $interval = $this->getLastUpdated()->diff(new \DateTime('now'));
            if (strlen($item->getContent()) <= 1000 && $item->getCreatedAt() && $interval->i <= 30)   {
                return $item;
            }
        }
        return null;
    }
}
