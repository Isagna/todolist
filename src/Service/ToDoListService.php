<?php


namespace App\Service;


use App\Entity\ToDoList;
use App\Entity\ToDoListItem;


class ToDoListService
{
    private $mailer;

    public function __construct(MailerService $mailerService)
    {
        $this->mailer = $mailerService;
    }

    public function addItem (ToDoList $toDoList, ToDoListItem $item)
    {
        if ($toDoList && $item) {
            if ($toDoList->canAddItem($item)) {
                $toDoList->addItem($item)->setLastUpdated(new \DateTime('now'));
                $this->mailer->send();
                return true;
            }
        }
        return false;
    }

    public function removeItem (ToDoList $toDoList, ToDoListItem $item)
    {
        if ($toDoList && $item) {
            $toDoList->removeItem($item);
            return true;
        }
        return false;
    }
}