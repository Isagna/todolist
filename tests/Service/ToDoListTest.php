<?php


namespace App\Tests\Service;

use App\Entity\ToDoList;
use App\Entity\ToDoListItem;
use App\Entity\User;
use PHPUnit\Framework\TestCase;


class ToDoListTest extends TestCase
{

    public function testAddItem ()
    {
        $user1 = new User();
        $user1->setBirth(new \DateTime('yesterday'))
            ->setEmail('user1@yopmail.com')
            ->setLastName('User')
            ->setFirstName('Mineur')
            ->setPlainPassword('azertyuiop')
        ;

        $toDoList = new ToDoList();
        $toDoList->setOwner($user1)
            ->setLastUpdated(new \DateTime('now'));


        $item = new ToDoListItem();
        $item->setCreatedAt(new \DateTime('now'))
            ->setName('Mon item')
            ->setContent('Lorem ipsum')
        ;

        $result = $toDoList->addItem($item);
        $this->assertEquals($toDoList,$result);
    }

    public function testAddItemError ()
    {
        $this->expectException(\TypeError::class);
        $user1 = new User();
        $user1->setBirth(new \DateTime('yesterday'))
            ->setEmail('user1@yopmail.com')
            ->setLastName('User')
            ->setFirstName('Mineur')
            ->setPlainPassword('azertyuiop')
        ;

        $toDoList = new ToDoList();
        $toDoList->setOwner($user1)
            ->setLastUpdated(new \DateTime('now'));


        $item = null;

        $result = $toDoList->addItem($item);
    }

    public function testRemoveItem () {
        $user1 = new User();
        $user1->setBirth(new \DateTime('yesterday'))
            ->setEmail('user1@yopmail.com')
            ->setLastName('User')
            ->setFirstName('Mineur')
            ->setPlainPassword('azertyuiop')
        ;

        $toDoList = new ToDoList();
        $toDoList->setOwner($user1)
            ->setLastUpdated(new \DateTime('now'));

        $initialToDoList = $toDoList;

        $item = new ToDoListItem();
        $item->setCreatedAt(new \DateTime('now'))
            ->setName('Mon item')
            ->setContent('Lorem ipsum')
        ;

        $toDoList->addItem($item);


        $result = $toDoList->removeItem($item);
        $this->assertEquals($initialToDoList,$result);

    }

    public function testRemoveItemError () {

        $this->expectException(\TypeError::class);

        $user1 = new User();
        $user1->setBirth(new \DateTime('yesterday'))
            ->setEmail('user1@yopmail.com')
            ->setLastName('User')
            ->setFirstName('Mineur')
            ->setPlainPassword('azertyuiop')
        ;

        $toDoList = new ToDoList();
        $toDoList->setOwner($user1)
            ->setLastUpdated(new \DateTime('now'));

        $item = null;

        $toDoList->removeItem($item);

    }

    public function testCanAddItem () {
        $user1 = new User();
        $user1->setBirth(new \DateTime('yesterday'))
            ->setEmail('user1@yopmail.com')
            ->setLastName('User')
            ->setFirstName('Mineur')
            ->setPlainPassword('azertyuiop')
        ;

        $toDoList = new ToDoList();
        $toDoList->setOwner($user1)
            ->setLastUpdated(new \DateTime('now'));


        $item = new ToDoListItem();
        $item->setCreatedAt(new \DateTime('now'))
            ->setName('Mon item')
            ->setContent('Lorem ipsum')
        ;

        $result = $toDoList->canAddItem($item);
        $this->assertEquals($item,$result);
    }
}