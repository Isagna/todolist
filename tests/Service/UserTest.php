<?php


namespace App\Tests\Service;

use App\Entity\ToDoList;
use App\Entity\ToDoListItem;
use App\Entity\User;
use PHPUnit\Framework\TestCase;


class UserTest extends TestCase
{

    public function testMineur ()
    {
        $user1 = new User();
        $user1->setBirth(new \DateTime('yesterday'))
            ->setEmail('user1@yopmail.com')
            ->setLastName('User')
            ->setFirstName('Mineur')
            ->setPlainPassword('azertyuiop')
        ;

        $result = $user1->isValid();
        $this->assertFalse($result);

    }

    public function testMajeur ()
    {
        $user3 = new User();
        $user3->setBirth(new \DateTime('1990-04-22'))
            ->setEmail('user3@yopmail.com')
            ->setLastName('')
            ->setFirstName('Majeur')
            ->setPlainPassword('azerty')
        ;

        $result = $user3->isValid();
        $this->assertFalse($result);

    }

    public function testOk ()
    {
        $user3 = new User();
        $user3->setBirth(new \DateTime('1990-04-22'))
            ->setEmail('user3@yopmail.com')
            ->setLastName('Est')
            ->setFirstName('Majeur')
            ->setPlainPassword('azertyuiop')
        ;

        $result = $user3->isValid();
        $this->assertTrue($result);

    }

}